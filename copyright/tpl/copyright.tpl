<!-- BEGIN: COPYRIGHT -->
	&copy; {CR_YEAR} <a href="{PHP.cfg.mainurl}" title="{PHP.cfg.maintitle} &ndash; {PHP.cfg.subtitle}">{PHP.cfg.maintitle}</a> {PHP.L.copyright_extra1}<span class="spaced">{PHP.cfg.separator}</span><a href="#" title="{PHP.L.copyright_extra5}">{PHP.L.copyright_extra4}</a>
<!-- END: COPYRIGHT -->