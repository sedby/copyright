<?php
/* ====================
[BEGIN_COT_EXT]
Hooks=footer.tags
Tags=footer.tpl:{FOOTER_PLUG_COPYRIGHT}
[END_COT_EXT]
==================== */

/**
 * Copyright Plugin for Cotonti CMF
 *
 * @version 2.10
 * @author Seditio.by
 * @copyright (c) 2008-2015 Seditio.by
 */

defined('COT_CODE') or die("Wrong URL.");

require_once(cot_langfile('copyright'));
$tt = new XTemplate(cot_tplfile('copyright', 'plug'));

if (!$plu_copyright)
{
	$year = date("Y", $sys['now']);
	if ((int)$cfg['plugin']['copyright']['fyear'] > 1980)
	{
		$year2 = $cfg['plugin']['copyright']['fyear'];
	}
	else
	{
		$row = $db->query("SELECT user_regdate FROM $db_users ORDER BY user_regdate ASC LIMIT 1")->fetch();
		$year2 = date("Y", $row['user_regdate']);
	}
	$year = ($year != $year2) ? $year2.' &ndash; '.$year.' '.$L['copyright_extra3'] : $year.' '.$L['copyright_extra2'];
	$tt->assign("CR_YEAR", $year);
	$tt->parse("COPYRIGHT");
	$plu_copyright = $tt->text("COPYRIGHT");
	$cache && $cache->db->store('plu_copyright', $plu_copyright, 'system', $cfg['plugin']['copyright']['cache']);
}

$t->assign("FOOTER_PLUG_COPYRIGHT", $plu_copyright);