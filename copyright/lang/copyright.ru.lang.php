<?php
/**
 * Russian Language File Copyright Plugin
 *
 * @version 2.10
 * @author Seditio.by
 * @copyright (c) 2008-2015 Seditio.by
 */

defined('COT_CODE') or die('Wrong URL.');

$L['info_desc'] = 'Размещение записи об авторском праве, автоматизированный счетчик лет';

/**
 * Plugin Config
 */

$L['cfg_fyear'] = array('Год начала копирайта','Не заполнять для использования даты регистрации первого пользователя');

/**
 * Plugin Body
 */

$L['copyright_extra1'] = "Все права защищены";
$L['copyright_extra2'] = "г.";
$L['copyright_extra3'] = "гг.";
$L['copyright_extra4'] = "Разработка сайта";
$L['copyright_extra5'] = "Информация о разработчике";