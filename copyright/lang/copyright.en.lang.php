<?php
/**
 * English Language File Copyright Plugin
 *
 * @version 2.10
 * @author Seditio.by
 * @copyright (c) 2008-2015 Seditio.by
 */

defined('COT_CODE') or die('Wrong URL.');

$L['info_desc'] = 'Place copyright notice and automated year counter';

/**
 * Plugin Config
 */

$L['cfg_fyear'] = array('Copyright starting year','Leave empty to use date of first user registration');

/**
 * Plugin Body
 */

$L['copyright_extra1'] = "All Rights Reserved";
$L['copyright_extra2'] = "";
$L['copyright_extra3'] = "";
$L['copyright_extra4'] = "Developer";
$L['copyright_extra5'] = "Developer Info";