<?php
/* ====================
[BEGIN_COT_EXT]
Code=copyright
Name=Copyright
Description=Generates copyright notice in footer.tpl
Version=2.10
Date=26-Feb-2015
Author=Seditio.by
Copyright=&copy; 2008 - 2015 Seditio.by
Notes=Once placed in the footer.tpl the tag will take care of the copyright notice, years & other information you can specify in the tpl/copyright.tpl<br />Unless you specify forced starting year value in the plugin config, the year of the first user account registration will be considered as the starting year
SQL=
Auth_guests=R
Lock_guests=W12345A
Auth_members=R
Lock_members=W12345A
[END_COT_EXT]

[BEGIN_COT_EXT_CONFIG]
cache=01:string::36000:Cache clear interval, sec
fyear=02:string:::Forced year value
[END_COT_EXT_CONFIG]
==================== */

defined('COT_CODE') or die("Wrong URL.");